<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product([
            'imagePath' => 'https://images.solefootgear.com/800-large_default/adidas-yeezy-boost-350-v2-semi-frozen-yellow.jpg',
            'title' => 'Yeezy Boost 350 V2',
            'description' => 'Semi Frozen Yellow',
            'price' => 14000
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'https://i5.walmartimages.com/asr/80092848-3049-46ba-8332-6b2b360a8b4b_1.4ac29fb319bcbb8bfbf705e84b6b4ed1.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF',
            'title' => 'AIR JORDAN',
            'description' => 'JORDAN 1 RETRO HIGH OFF-WHITE "OFF WHITE UNC"',
            'price' => 33500
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'https://cdn-images.farfetch-contents.com/12/15/66/20/12156620_10148186_480.jpg',
            'title' => 'GUCCI',
            'description' => 'Ace embroidered sneaker',
            'price' => 22000
        ]);
        $product->save();


    }
}
