<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="{{ route('product.index') }}"><span class="glyphicon glyphicon-home"></span> Guitar Shop</a>


  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="{{ route('product.shoppingCart') }}">
              <span class="glyphicon glyphicon-shopping-cart"></span> ตะกร้าสินค้า
            <span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
          </a>
        </li>
      </ul>


      <ul class="nav navbar-nav navbar-right">
      <li class="nav-item dropdown ">
        <a class=" dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="glyphicon glyphicon-user"></span> ผู้ใช้</a>
        <ul class="dropdown-menu" >
          @if(Auth::check())
            <a class="dropdown-item" href="{{ route('user.profile') }}">รายการสั่งซื้อ</a>
                {{--<a class="dropdown-item" href="{{ route('product.add') }}">เพิ่มสินค้า</a>--}}
                <a class="dropdown-item" href="{{ route('user.update') }}">ข้อมูลส่วนตัว</a>
            <a class="dropdown-item" href="{{ route('user.logout') }}">ออกจากระบบ</a>
            @else
                <a class="dropdown-item" href="{{ route('user.signup') }}">สมัครสมาชิก</a>
             <a class="dropdown-item" href="{{ route('user.signin') }}">ลงชื่อเข้าใช้</a>
            @endif
        </ul>
      </li>
      </ul>

  </div>
</nav>