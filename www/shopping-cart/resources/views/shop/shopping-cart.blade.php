@extends('layouts.master')

@section('title')
    Guitar Shopping
@endsection

@section('content')
    @if(Session::has('cart'))
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <ul class="list-group">
                    @foreach($products as $product)
                        <li class="list-group-item">
                            <strong>{{ $product['item']['title'] }}</strong>
                            <span class="badge">{{ $product['qty'] }} คู่</span>
                            <span class="label label-success">ราคา {{ $product['price'] }} บาท</span>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown"> ลบ
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('product.reduceByOne', ['id' => $product['item']['id']]) }}">ลบ 1 รายการ</a></li>
                                    <li><a href="{{ route('product.remove', ['id' => $product['item']['id']]) }}">ลบทั้งหมด</a></li>

                                </ul>
                            </div>

                        </li>

                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
              <strong>ยอดรวม: {{ $totalPrice }} บาท</strong>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <a href="{{ route('checkout') }}" ><button type="submit" class="btn btn-success">จ่ายเงิน</button></a>
            </div>
        </div>
       @else
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <h2>ไม่มีสินค้าในตะกร้า !</h2>
            </div>
        </div>
    @endif
@endsection