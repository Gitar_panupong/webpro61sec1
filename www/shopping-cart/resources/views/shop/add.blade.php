@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h1>แก้ไขข้อมูล</h1>
            <hr>
            <h4>User id: {{ $email = Auth::user()->id }}</h4>
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif


            <form action="{{ route('product.add') }}" method="post">
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="text" id="email" name="email" class="form-control" value="{{ $email = Auth::user()->email }}">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="text" id="password" name="password" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary"> ตกลง</button>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
@endsection