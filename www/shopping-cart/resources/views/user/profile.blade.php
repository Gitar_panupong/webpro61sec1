@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>ประวัติการสั่งซื้อ</h1>

            @foreach($orders as $order)
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach($order->cart->items as $item)
                        <li class="list-group-item">ยี่ห้อ :
                           {{ $item['item']['title'] }} <hr> รุ่น : {{ $item['item']['description'] }}  <hr> จำนวน  {{ $item['qty'] }} คู่  <span class="badge">{{ $item['price'] }} บาท </span> <hr>
                        </li>
                            @endforeach

                    </ul>
                </div>
                <div class="panel-footer">
                    <strong>ราคารวม: {{ $order->cart->totalPrice }} บาท</strong>
                </div>
            </div>
                @endforeach
        </div>
    </div>
@endsection