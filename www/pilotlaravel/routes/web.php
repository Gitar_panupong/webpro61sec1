<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
   //return "Hello World";

    $user = new \App\User();
    $user ->setAttribute("Name","Guitar");
    $user ->setAttribute("Email","Guitar@hotmail.com");
    $user ->setAttribute("Password",Hash::make('12345'));

    $user->save();
    return "The user is saved";

});
