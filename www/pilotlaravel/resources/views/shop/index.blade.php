@extends('layouts.master')

@section('title')
    Guitar Shopping
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="https://stockx-360.imgix.net/Adidas-Yeezy-Boost-350-V2-Semi-Frozen-Yellow/Images/Adidas-Yeezy-Boost-350-V2-Semi-Frozen-Yellow/Lv2/img01.jpg?auto=format,compress&w=1117&q=90&updated_at=1538080256" alt="..." class="img-responsive">
                <div class="caption">
                    <h3>adidas Yeezy Boost 350 V2 Semi Frozen Yellow</h3>
                    <p class="description">The Semi Frozen Yellow adidas Yeezy Boost 350 V2 was rumored as early as May of 2017-some blogs even claimed they would be mostly glow-in-the-dark-but it was when photos of Kanye West wearing them first surfaced on the web later in the year that confirmed the release. This colorway steps outside of the comfort zone of previous 350 Yeezy sneaker releases, which have remained subtle and mostly neutral primary colors and bright accents. The official colorway of the 'Frozen Yellow Yeezys' is actually Semi Frozen Yellow, Raw Steel and Red and features both of adidas pinnacle technologies, Boost cushioning and Primeknit sock-like upper. The hype for the yellow </p>
                    <div class="clearfix">
                        <div class="pull-left price">14000.00 บาท</div>
                        <a href="#" class="btn btn-success pull-right" role="button">Add to Cart</a>
                    </div>
                </div>
            </div>
        </div>
@endsection
