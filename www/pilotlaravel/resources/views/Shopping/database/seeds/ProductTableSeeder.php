<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $product = new \App\Product([
            'imagePath' => 'https://stockx-360.imgix.net/Adidas-Yeezy-Boost-350-V2-Semi-Frozen-Yellow/Images/Adidas-Yeezy-Boost-350-V2-Semi-Frozen-Yellow/Lv2/img01.jpg?auto=format,compress&w=1117&q=90&updated_at=1538080256',
            'title' => 'adidas Yeezy Boost 350 V2 Semi Frozen Yellow',
            'description' => 'Super Cool',
            'price' => 14000
        ]);
        $product->save();

    }
}
