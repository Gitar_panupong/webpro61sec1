function makeUser(nameValue,ageValue,addValue,telValue,sexValue,emailValue) {
	return {name : nameValue,
			age : ageValue,
			address : addValue,
			telno : telValue,
			sex : sexValue,
			email : emailValue};
}

function showUser(user){
	let str = "";
	for(let key in user){
		str = str + user[key]+"\n";
	}
	return str;
}

function cloneUser(user){
	let tmpUser = {};
	for(let key in user){
		tmpUser[key] = user[key];
	}
	return tmpUser;
}